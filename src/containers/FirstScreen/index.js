import React, { Component } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { Header, Left, Right, Icon, Button } from 'native-base'

export default class FirstScreen extends Component {


    render() {

        return (
            <View style={styles.container}>
                <Header>
                    <Left>
                        <Button transparent>
                            <Icon name="menu" onPress={() => { this.props.navigation.openDrawer() }} />
                        </Button>
                    </Left>
                    <Right />
                </Header>
         
                <Text>
                    first screen
                 </Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})


