/** @format */

import {AppRegistry,StatusBar} from 'react-native';
import App from './src/app';
import {name as appName} from './app.json';
StatusBar.setHidden(true)
AppRegistry.registerComponent(appName, () => App);
